if( $args.Length -lt 1 )
{
	"Pass in appids as arguments: 440 570 730"
	Exit 1
}

foreach( $AppID in $args )
{
	# Parse appids from links
	$AppID = $AppID -creplace '.*?/([0-9]+).*', '$1'

	"App $AppID"

	$env:SteamAppId = $AppID
	Start-Process Rundll32 steam_api.dll,SteamAPI_Init -Wait
}

"Done"
